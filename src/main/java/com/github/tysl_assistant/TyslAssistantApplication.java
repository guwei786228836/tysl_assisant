package com.github.tysl_assistant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;


@EnableAsync
@SpringBootApplication
public class TyslAssistantApplication {

    public static void main(String[] args) {
        SpringApplication.run(TyslAssistantApplication.class, args);
    }

}
