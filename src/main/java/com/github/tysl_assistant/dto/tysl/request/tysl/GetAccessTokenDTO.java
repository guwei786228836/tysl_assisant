package com.github.tysl_assistant.dto.tysl.request.tysl;

import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

/**
*@Description: TODO
*@Author: caijinyu
*@CreateTime: 2023-07-05  20:5234
*/
@Data
@FieldNameConstants
public class GetAccessTokenDTO extends TyslRequestDTO {

    /**
     * 根据获取令牌或者刷新令牌场景选择以下两个值之一：
     * "vcp_189"：平台应用获取访问令牌
     * "refresh_token"：平台应用刷新令牌
     */
    private String grantType;
    /**
     * 刷新token时必传，非刷新token时参数不传；
     */
    private String refreshToken;

}
