package com.github.tysl_assistant.dto.tysl.request.tysl;


import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class BatchDeviceStatusDTO extends TyslRequestDTO {
    /**
     * 设备码，多个设备码逗号隔开，最多 20 个设备
     */
    private String deviceCodes;

    /**
     * 查询状态（1.在线状态；2.云存状态；3.绑定状态）不传值默认在线状态
     */
    private Integer queryData;
}
