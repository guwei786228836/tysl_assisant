package com.github.tysl_assistant.dto.tysl.response;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * @Description: TODO
 * @Author: caijinyu
 * @CreateTime: 2023-07-06  08:2213
 */
@Data
public class TokenResponseDTO {

    private String accessToken;
    private String refreshToken;
    private Integer expiresIn;
    private Integer refreshExpiresIn;

    private LocalDateTime expireLocalDateTime;
}
