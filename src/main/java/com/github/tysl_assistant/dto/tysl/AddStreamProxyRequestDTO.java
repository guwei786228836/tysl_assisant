package com.github.tysl_assistant.dto.tysl;

import lombok.Data;

@Data
public class AddStreamProxyRequestDTO {

    /**
     * 流id
     */
    private String id;

    /**
     * 摄像头code
     */
    private String code;

    /**
     * 推流开始时间（YYYY-MM-DD hh:mm:ss）
     */
    private String requestTime;

    /**
     * 推流速度
     */
    private Double speed;

}
