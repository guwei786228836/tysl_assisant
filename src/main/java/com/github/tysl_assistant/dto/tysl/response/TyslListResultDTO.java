package com.github.tysl_assistant.dto.tysl.response;

import lombok.Data;

import java.util.List;

@Data
public class TyslListResultDTO<T> {

    private  Integer pageNo;

    private Integer pageSize;

    private Integer totalCount;

    private List<T> list;
}
