package com.github.tysl_assistant.dto.tysl;

import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class TyslRequestDTO {
    /**
     * 接入 appId
     */
    protected String appId;

    /**
     * 签名算法：
     * signature =HMAC-SHA256(appId+
     * clientType+params+timestamp+ve
     * rsion，appSecret)
     * 备注：注意保持签名的各字段顺序正确
     */
    protected String signature;

    /**
     * 使用 appSecret 对所有传入参数采
     * 用 XXTea 加密，并且按照接口详细
     * 规范中定义的参数(除 signature)
     * 拼接，不要求参数顺序。
     * 例如：params =
     * XXTea((a=value1&b=value2&…),a
     * ppSecret)
     */
    protected String params;

    /**
     * 服务端版本号，比如 v1.0
     */
    protected String version;
    /**
     * 端类型，3:pc
     */
    protected String clientType;

    /**
     * 当前 UTC 时间戳，从 1970 年 1 月 1 日 0 点 0 分 0 秒开始到现在的毫
     * 秒数，如 1561087169952
     */
    protected String timestamp;

    /**
     * 绑定手机号码
     */
    protected String phone;
}
