package com.github.tysl_assistant.dto.tysl.request.tysl;

import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class GetCloudFolderListRequestDTO extends TyslRequestDTO {

    private String deviceCode;

    private String path;

    private String startDate;

    private String endDate;

    private String type;
}
