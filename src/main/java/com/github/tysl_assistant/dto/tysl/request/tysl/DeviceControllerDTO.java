package com.github.tysl_assistant.dto.tysl.request.tysl;

import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

/**
 * @Description: 设备控制入参
 * @Author: caijinyu
 * @CreateTime: 2023-07-05  16:2343
 */
@Data
@FieldNameConstants
public class DeviceControllerDTO extends TyslRequestDTO {

    private String accessToken;
    /**
     * 设备code
     */
    private String deviceCode;

    /**
     * 0-开始1-停止
     */
    private Integer action;

    /**
     * 控制指令
     */
    private String ptzCmd;

    /**
     * 速度 1-100 默认50
     */
    private Integer speed;

    /**
     * 通道号 默认0
     */
    private Integer channel;
}
