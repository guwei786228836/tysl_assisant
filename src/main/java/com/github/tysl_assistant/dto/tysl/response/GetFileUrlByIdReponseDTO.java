package com.github.tysl_assistant.dto.tysl.response;

import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class GetFileUrlByIdReponseDTO {

   private String url;

   private  Long  seekSeconds;
}
