package com.github.tysl_assistant.dto.tysl.response;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.github.tysl_assistant.util.json.jackson.deserializer.CustomDateDeserializer;
import com.github.tysl_assistant.util.json.jackson.serializer.CustomDateSerializer;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

import java.util.Date;

@Data
@FieldNameConstants
public class GetCloudFileListReponseDTO {

    private String id;

    private String name;

    private String iconUrl;

    private Long size;

    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date createDate;

    private String fileType;

    @JsonSerialize(using = CustomDateSerializer.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private Date lastOpTime;
}
