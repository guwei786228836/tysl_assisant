package com.github.tysl_assistant.dto.tysl;

import lombok.Data;
import lombok.experimental.FieldNameConstants;

import java.util.Date;

@Data
@FieldNameConstants
public class GetNextFileNowDTO {

    /**
     * 流id
     */
    private String id;

    /**
     * 摄像头code
     */
    private String code;

    /**
     * 推流开始时间（YYYY-MM-DD hh:mm:ss）
     */
    private Date requestTime;
    /**
     * 推流文件间隔时间
     */
    private Date lastOpTime;

    /**
     * 推流速度
     */
    private Double speed;

    /**
     * rtp服务器 流id
     */
    private String  streamId;

    /**
     * 文件的index
     */
    private Integer index;

    /**
     * 目录的index
     */
    private Integer pathIndex;

}
