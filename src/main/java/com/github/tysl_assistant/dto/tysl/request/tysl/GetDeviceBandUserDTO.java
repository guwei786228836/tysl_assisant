package com.github.tysl_assistant.dto.tysl.request.tysl;

import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class GetDeviceBandUserDTO extends TyslRequestDTO {

    private String deviceCode;
}
