package com.github.tysl_assistant.dto.tysl.response;

import lombok.Data;

@Data
public class TyslResultDTO {
    private Integer code;
    private String msg;
    private String data;
}
