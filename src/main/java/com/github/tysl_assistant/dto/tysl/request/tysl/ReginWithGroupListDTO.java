package com.github.tysl_assistant.dto.tysl.request.tysl;

import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class ReginWithGroupListDTO extends TyslRequestDTO {

    private String enterpriseUser;

    private String regionId;
}
