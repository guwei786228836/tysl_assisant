package com.github.tysl_assistant.dto.tysl;

import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class GetNextFileRequsetDTO {

    /**
     * 流id
     */
    private String id;

    /**
     * 摄像头code
     */
    private String code;

    /**
     * 推流开始时间（YYYY-MM-DD hh:mm:ss）
     */
    private String requestTime;

    /**
     * 推流速度
     */
    private Double speed;

    /**
     * rtp服务器 流id
     */
    private String  streamId;

    /**
     * 进度条拖动，切换目录
     */
    private Boolean   changeSourceFlag = false;
}
