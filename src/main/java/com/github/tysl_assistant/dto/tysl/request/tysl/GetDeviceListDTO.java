package com.github.tysl_assistant.dto.tysl.request.tysl;

import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import lombok.Data;
import lombok.experimental.FieldNameConstants;

@Data
@FieldNameConstants
public class GetDeviceListDTO extends TyslRequestDTO {

    private String regionId;

    private Integer pageNo;

    private Integer pageSize;
}
