package com.github.tysl_assistant.dto.zlmedia;

import lombok.Data;

@Data
public class ZLRequestDTO {

    String app;

    String stream;

}
