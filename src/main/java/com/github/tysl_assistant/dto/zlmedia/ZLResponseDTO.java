package com.github.tysl_assistant.dto.zlmedia;

import lombok.Data;

@Data
public class ZLResponseDTO {

    Integer code;

    String msg;

}
