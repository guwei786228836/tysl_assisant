package com.github.tysl_assistant.dto.zlmedia.response;

import com.github.tysl_assistant.dto.zlmedia.ZLResponseDTO;
import lombok.Data;

@Data
public class ChangeMp4SeekZLResponseDTO extends ZLResponseDTO {

    private Integer result;

}
