package com.github.tysl_assistant.dto.zlmedia.response;

import com.github.tysl_assistant.dto.zlmedia.ZLResponseDTO;
import lombok.Data;

@Data
public class AddStreamProxyZLResponseDTO extends ZLResponseDTO {

    private AddStreamProxyZLResponseDataDTO data;

}
