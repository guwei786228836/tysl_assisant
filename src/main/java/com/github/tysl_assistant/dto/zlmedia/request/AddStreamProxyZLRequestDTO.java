package com.github.tysl_assistant.dto.zlmedia.request;

import com.github.tysl_assistant.dto.zlmedia.ZLRequestDTO;
import lombok.Data;

@Data
public class AddStreamProxyZLRequestDTO extends ZLRequestDTO {


    private String url;

}
