package com.github.tysl_assistant.util.json.jackson.serializer;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;

public class IntegerToStringSerialize extends JsonSerializer<Integer> {

    public IntegerToStringSerialize() {
    }

    @Override
    public void serialize(Integer value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        if(value == null) {
            jsonGenerator.writeString("");
        }else {
            jsonGenerator.writeString(String.valueOf(value));
        }
    }
}
