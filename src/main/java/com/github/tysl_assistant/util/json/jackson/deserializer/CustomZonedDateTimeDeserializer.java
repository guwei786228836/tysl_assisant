package com.github.tysl_assistant.util.json.jackson.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class CustomZonedDateTimeDeserializer extends JsonDeserializer<ZonedDateTime> {


    @Override
    public ZonedDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) {
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").withZone(ZoneId.of("GMT+08:00"));
        ZonedDateTime date = null;
        try {
            String text = jsonParser.getText();
            text = text.length() == 10 ? (text + " 00:00:00") : text;
            date = ZonedDateTime.parse(text, df);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return date;
    }
}
