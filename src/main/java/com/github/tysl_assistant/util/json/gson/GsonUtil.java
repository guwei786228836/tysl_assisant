package com.github.tysl_assistant.util.json.gson;

import com.google.gson.Gson;
import com.google.gson.JsonElement;

import java.util.Collections;
import java.util.Map;

public class GsonUtil {


    public static final Gson GSON = new Gson();

    /**
     * gson 实体转json
     *
     * @param obj
     * @return
     */
    public static String objectToString(Object obj) {
        return GSON.toJson(obj);
    }

    /**
     * gson 实体转Map
     *
     * @param obj
     * @return
     */
    public static Map<String, Object> objectToMap(Object obj) {
        if (obj == null) {
            return Collections.emptyMap();
        }
        return GSON.fromJson(GSON.toJson(obj), Map.class);
    }


    /**
     * gson json转实体
     *
     * @param json
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T string2Bean(String json, Class<T> clazz) {
        return GSON.fromJson(json, clazz);
    }

    /**
     * gson map转实体
     *
     * @param map
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> T map2Bean(Map<?, ?> map, Class<T> clazz) {
        String paramJsonString = GSON.toJson(map);
        return GSON.fromJson(paramJsonString, clazz);
    }

    /**
     * gson 实体转JsonObject
     *
     * @param obj
     * @return
     */
    public static JsonElement bean2JsonObject(Object obj) {
        return GSON.toJsonTree(obj);
    }
}
