package com.github.tysl_assistant.util.secure;

import cn.hutool.crypto.symmetric.XXTEA;

public class XXTeaUtil {

    /**
     * XXTea加密
     * @param s
     * @return
     */
    public static String  encrypt(String key,String s){
        XXTEA xxtea = new XXTEA(key.getBytes());
        return  xxtea.encryptHex(s);
    }

}
