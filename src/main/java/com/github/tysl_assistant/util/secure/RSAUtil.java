package com.github.tysl_assistant.util.secure;

import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.HexUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.asymmetric.KeyType;
import cn.hutool.crypto.asymmetric.RSA;

public class RSAUtil {

    /**
     * RSA私钥解密
     * @param RSAPrikey
     * @param s
     * @return
     */
    public static  String decrypt(String RSAPrikey,String s){
        RSA rsa = new RSA(RSAPrikey, null);
        byte[] aByte = HexUtil.decodeHex(s);
        byte[] decrypt = rsa.decrypt(aByte, KeyType.PrivateKey);
        return  StrUtil.str(decrypt, CharsetUtil.CHARSET_UTF_8);
    }
}
