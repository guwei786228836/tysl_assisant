package com.github.tysl_assistant.util;

public interface CommonConstants {

    interface MediaType {
        /**
         * 高清
         */
        Integer MEDIA_TYPE_720 = 0;
        /**
         * 标清
         */
        Integer MEDIA_TYPE_360 = 1;
    }

    interface Common {
        int ONE  = 1;
        int TEN_THOUSAND  = 100000;
        /**
         * utf-8
         */
        String UTF8 = "utf-8";
        /**
         * 逗号
         */
        String COMMA = ",";

        /**
         * 冒号
         */
        String COLON = ":";
        /**
         * 斜杠
         */
        String SLASH = "/";
        /**
         * 双斜杠
         */
        String DOUBLE_SLASH = "//";
        /**
         * 分页首页
         */
        int FIRST_PAGE_NUM = 1;
        /**
         * 分页默认页大小
         */
        int PAGE_SIZE = 50;

        /**
         * 环境
         */
        String PROD = "prod";

        /**
         * 横杠
         */
        String RUNG = "-";

        /**
         * 等于号
         */
        String EQUAL = "=";

        /**
         *
         */
        String AND = "&";

    }

    interface oos {
        String FILE_SEPARATOR = "/";
        String IMAGE_SCALE = "scale";
        String PDF_PATH = "pdf";
        String MEDICALINSURANCE_PATH = "medicalInsurance";
    }

    interface  videoReview{
        /**
         * 回看文件名称为开始时间与结束时间，取时间的长度
         */
        int  FILE_NAME_TIME_LIMITS = 29;

        /**
         * 分钟
         */
        int MINUTE = 60000;

        /**
         * 毫秒
         */
        int MILLISECOND = 1000;

        /**
         * 毫秒格式化
         */
        String MILLISECOND_FORMAT = ".000";

        /**
         * 时间进制
         */
        int   TIME_BASE = 60;

        /**
         * 视屏回看 取文件进一秒
         */
        int ONE_SECOND = 1;
    }

    interface Cn21Status {
        /**
         * 接口成功标识
         */
        Integer SUCESS_CODE = 0;

        /**
         * 时间正序
         */
        Integer ORDER_BY_ASC = 2;

        /**
         * 按 时间段搜索
         */
        Integer  SEARCH_BY_TIME = 2;
    }


    /**
     * http接口状态码
     */
    interface RestfulStatus {
        /**
         * controller层返回成功标识
         */
        Integer SUCESS_CODE = 200;
        /**
         * controller层返回失败标识
         */
        Integer FAILE_CODE = -1;

        /**
         * controller层返回成功备注
         */
        String SUCESS_MSG = "成功";

        /**
         * controller层返回失败备注
         */
        String FAIL_MSG = "失败";
    }


    /**
     * redis一些参数
     */
    interface RedisContants {
        /**
         * 冒号
         */
        String COLON = ":";
        /**
         * 星号
         */
        String ASTERISK = "*";
        /**
         * 百分号
         */
        String PERCENT = "%";
        /**
         * token
         */
        String TOKEN_COLON = "token" + RedisContants.COLON;
        /**
         * token
         */
        String TOKEN = "token";
        /**
         * token失效时间
         */
        Integer EXPIRE_TIME = 3600;
        /**
         * 生效时间20分钟
         */
        int EXPIRED_20_MINUTES = 1200;

        /**
         * 字典
         */
        String VIDEO_SERVER_TOKEN = "video_server_token";
    }


    /**
     * http状态码
     */
    interface HttpStatus {
        int SC_CONTINUE = 100;
        int SC_SWITCHING_PROTOCOLS = 101;
        int SC_PROCESSING = 102;
        int SC_OK = 200;
        int SC_CREATED = 201;
        int SC_ACCEPTED = 202;
        int SC_NON_AUTHORITATIVE_INFORMATION = 203;
        int SC_NO_CONTENT = 204;
        int SC_RESET_CONTENT = 205;
        int SC_PARTIAL_CONTENT = 206;
        int SC_MULTI_STATUS = 207;
        int SC_MULTIPLE_CHOICES = 300;
        int SC_MOVED_PERMANENTLY = 301;
        int SC_MOVED_TEMPORARILY = 302;
        int SC_SEE_OTHER = 303;
        int SC_NOT_MODIFIED = 304;
        int SC_USE_PROXY = 305;
        int SC_TEMPORARY_REDIRECT = 307;
        int SC_BAD_REQUEST = 400;
        int SC_UNAUTHORIZED = 401;
        int SC_PAYMENT_REQUIRED = 402;
        int SC_FORBIDDEN = 403;
        int SC_NOT_FOUND = 404;
        int SC_METHOD_NOT_ALLOWED = 405;
        int SC_NOT_ACCEPTABLE = 406;
        int SC_PROXY_AUTHENTICATION_REQUIRED = 407;
        int SC_REQUEST_TIMEOUT = 408;
        int SC_CONFLICT = 409;
        int SC_GONE = 410;
        int SC_LENGTH_REQUIRED = 411;
        int SC_PRECONDITION_FAILED = 412;
        int SC_REQUEST_TOO_LONG = 413;
        int SC_REQUEST_URI_TOO_LONG = 414;
        int SC_UNSUPPORTED_MEDIA_TYPE = 415;
        int SC_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
        int SC_EXPECTATION_FAILED = 417;
        int SC_INSUFFICIENT_SPACE_ON_RESOURCE = 419;
        int SC_METHOD_FAILURE = 420;
        int SC_UNPROCESSABLE_ENTITY = 422;
        int SC_LOCKED = 423;
        int SC_FAILED_DEPENDENCY = 424;
        int SC_INTERNAL_SERVER_ERROR = 500;
        int SC_NOT_IMPLEMENTED = 501;
        int SC_BAD_GATEWAY = 502;
        int SC_SERVICE_UNAVAILABLE = 503;
        int SC_GATEWAY_TIMEOUT = 504;
        int SC_HTTP_VERSION_NOT_SUPPORTED = 505;
        int SC_INSUFFICIENT_STORAGE = 507;
    }

}
