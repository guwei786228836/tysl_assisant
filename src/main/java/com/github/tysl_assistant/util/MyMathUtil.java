package com.github.tysl_assistant.util;

public class MyMathUtil {

    public static int randomA2b(int a, int b) {
        return (int) (Math.random() * (b - a)) + a;
    }

    public static int generateMaxByteNum() {
        return MyMathUtil.randomA2b(1000000000, Integer.MAX_VALUE);
    }
}
