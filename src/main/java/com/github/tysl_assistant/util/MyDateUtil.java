package com.github.tysl_assistant.util;

import cn.hutool.core.date.DateTime;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Locale;

public class MyDateUtil {

    public static String DATE_TIME_FORMAT_STANDARD = "yyyy-MM-dd HH:mm:ss";

    public static String DATE_TIME_FORMAT_STANDARD_WITHOUT_SECOND_OTHER = "yyyyMMddHHmm";

    public static String DATE_TIME_FORMAT_STANDARD_OTHER = "yyyyMMddHHmmss";

    public static String DATE_TIME_FORMAT_FULL = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    public static final String yyyy_MM_dd_T_HH_mm_ss = "yyyy-MM-dd'T'HH:mm:ss";

    public static String DATE_TIME_FORMAT_WITHOUT_SECOND_AND_BAR = "yyyyMMddHHmm";

    public static String DATE_FORMAT_WITHOUT_HORIZONTAL_BAR = "yyyyMMdd";

    public static String TIME_FORMAT_STANDARD = "HH:mm:ss";

    public static String TIME_FORMAT_WITHOUT_SECOND = "HH:mm";

    public static String DATE_FORMAT_STANDARD = "yyyy-MM-dd";

    public static String localDate2String(LocalDate localDate) {
        if (null == localDate) {
            return null;
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return localDate.format(fmt);
    }

    public static String localDateTime2String(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(MyDateUtil.DATE_TIME_FORMAT_STANDARD);
        return localDateTime.format(fmt);
    }

    public static String localTime2String(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(MyDateUtil.TIME_FORMAT_STANDARD);
        return localDateTime.format(fmt);
    }

    public static String localDateTime2SimpleString(LocalDateTime localDateTime) {
        if (null == localDateTime) {
            return null;
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern(MyDateUtil.DATE_FORMAT_STANDARD);
        return localDateTime.format(fmt);
    }

    public static Date DateTime2Date(DateTime dateTime) {
        if (null == dateTime) {
            return null;
        }
        long time = dateTime.getTime();
        return new Date(time);
    }

    public static String ZonedDateTime2SimpleString(ZonedDateTime zonedDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MyDateUtil.DATE_TIME_FORMAT_STANDARD).withZone(ZoneId.systemDefault());
        String formattedDate = formatter.format(zonedDateTime);
        return formattedDate;
    }

    public static String ZonedDateTime2DateString(ZonedDateTime zonedDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MyDateUtil.DATE_FORMAT_STANDARD).withZone(ZoneId.systemDefault());
        String formattedDate = formatter.format(zonedDateTime);
        return formattedDate;
    }


    public static String ZonedDateTime2TimeString(ZonedDateTime zonedDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MyDateUtil.TIME_FORMAT_STANDARD).withZone(ZoneId.systemDefault());
        String formattedDate = formatter.format(zonedDateTime);
        return formattedDate;
    }

    public static String ZonedDateTime2TimeWithouSecondString(ZonedDateTime zonedDateTime) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(MyDateUtil.TIME_FORMAT_WITHOUT_SECOND).withZone(ZoneId.systemDefault());
        String formattedDate = formatter.format(zonedDateTime);
        return formattedDate;
    }

    public static String yyyyMMddHHmmss2ISO8601(String formatTime) {

        SimpleDateFormat oldsdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARD, Locale.getDefault());
        SimpleDateFormat newsdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARD, Locale.getDefault());
        try {
            return newsdf.format(oldsdf.parse(formatTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String ISO8601ToyyyyMMddHHmmss(String formatTime) {

        SimpleDateFormat oldsdf = new SimpleDateFormat(yyyy_MM_dd_T_HH_mm_ss, Locale.getDefault());
        SimpleDateFormat newsdf = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARD, Locale.getDefault());
        try {
            return newsdf.format(oldsdf.parse(formatTime));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static long yyyyMMddHHmmss2Timestamp(String formatTime) {
        SimpleDateFormat format = new SimpleDateFormat(DATE_TIME_FORMAT_STANDARD);
        //设置要读取的时间字符串格式
        Date date;
        try {
            date = format.parse(formatTime);
            Long timestamp = date.getTime() / 1000;
            //转换为Date类
            return timestamp;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
