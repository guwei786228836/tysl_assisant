package com.github.tysl_assistant.util.ajax;

import lombok.Data;

@Data
public class AjaxResponse {

    private Integer code;
    private String msg;
    private Object data;

}
