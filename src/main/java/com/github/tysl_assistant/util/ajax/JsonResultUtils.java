package com.github.tysl_assistant.util.ajax;


import com.github.tysl_assistant.util.CommonConstants;

public class JsonResultUtils {

	public static AjaxResponse build(Integer code, String msg) {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(code);
		ajaxResponse.setMsg(msg);
		return ajaxResponse;
	}

	public static AjaxResponse build(Integer code, String msg, Object result) {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(code);
		ajaxResponse.setMsg(msg);
		ajaxResponse.setData(result);
		return ajaxResponse;
	}

	public static AjaxResponse buildSucess() {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(CommonConstants.RestfulStatus.SUCESS_CODE);
		ajaxResponse.setMsg(CommonConstants.RestfulStatus.SUCESS_MSG);
		return ajaxResponse;
	}

	public static AjaxResponse buildSucess(Object result) {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(CommonConstants.RestfulStatus.SUCESS_CODE);
		ajaxResponse.setMsg(CommonConstants.RestfulStatus.SUCESS_MSG);
		ajaxResponse.setData(result);
		return ajaxResponse;
	}

	public static AjaxResponse buildFail(String msg, Object result) {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(CommonConstants.RestfulStatus.FAILE_CODE);
		ajaxResponse.setMsg(msg);
		ajaxResponse.setData(result);
		return ajaxResponse;
	}

	public static AjaxResponse buildFail(String msg) {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(CommonConstants.RestfulStatus.FAILE_CODE);
		ajaxResponse.setMsg(msg);
		return ajaxResponse;
	}


	public static AjaxResponse buildFail(Object result) {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(CommonConstants.RestfulStatus.FAILE_CODE);
		ajaxResponse.setMsg(CommonConstants.RestfulStatus.FAIL_MSG);
		ajaxResponse.setData(result);
		return ajaxResponse;
	}

	public static AjaxResponse buildFail() {
		AjaxResponse ajaxResponse = new AjaxResponse();
		ajaxResponse.setCode(CommonConstants.RestfulStatus.FAILE_CODE);
		ajaxResponse.setMsg(CommonConstants.RestfulStatus.FAIL_MSG);
		return ajaxResponse;
	}
}
