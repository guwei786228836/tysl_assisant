package com.github.tysl_assistant.controller;

import com.github.tysl_assistant.dto.tysl.AddStreamProxyRequestDTO;
import com.github.tysl_assistant.dto.tysl.response.GetFileUrlByIdReponseDTO;
import com.github.tysl_assistant.service.TyslService;
import com.github.tysl_assistant.util.ajax.AjaxResponse;
import com.github.tysl_assistant.util.ajax.JsonResultUtils;
import com.github.tysl_assistant.util.json.jackson.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.io.IOException;

@Controller
@RestController
@RequestMapping("/tysl")
@Slf4j
public class TyslController {


    @Resource
    private TyslService tyslService;


    /**
     * @param addStreamProxyRequestDTO
     * @return
             */
    @GetMapping("/addStreamProxy")
    @ResponseBody
    public AjaxResponse addStreamProxy(AddStreamProxyRequestDTO addStreamProxyRequestDTO){
        log.info("addStreamProxy begin:"+ JacksonUtil.objectToString(addStreamProxyRequestDTO));
        try {
            GetFileUrlByIdReponseDTO reponseDTO =  tyslService.addStreamProxy(addStreamProxyRequestDTO);
           if(null != reponseDTO){
               return JsonResultUtils.buildSucess(reponseDTO);
           }
        } catch (IOException e) {
            e.printStackTrace();
            log.info("addStreamProxy错误",e);
        }
        return JsonResultUtils.buildFail(null);
    }


}
