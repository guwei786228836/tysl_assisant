package com.github.tysl_assistant.io;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface TyslServer {

    /**
     * 获取目录树
     * @param info
     * @return
     */
    @POST("/open/u/device/getReginWithGroupList")
    Call<ResponseBody> reginWithGroupList(@Body RequestBody info);

    /**
     * 获取设备信息
     * @param info
     * @return
     */
    @POST("/open/u/device/showDevice")
    Call<ResponseBody> showDevice(@Body RequestBody info);

    /**
     * 批量查询设备状态
     * @param info
     * @return
     */
    @POST("/open/u/device/batchDeviceStatus")
    Call<ResponseBody> batchDeviceStatus(@Body RequestBody info);

    @POST("/open/u/device/batchRegionCount")
    Call<ResponseBody> batchRegionCount(@Body RequestBody info);

    @POST("/open/u/device/getDeviceList")
    Call<ResponseBody> getDeviceList(@Body RequestBody info);

    @POST("/open/u/device/getDeviceBandUser")
    Call<ResponseBody> getDeviceBandUser(@Body RequestBody info);

    @POST("/open/u/device/getDeviceResource")
    Call<ResponseBody> getDeviceResource(@Body RequestBody info);

    @POST("/open/u/cloud/getDeviceMediaUrl")
    Call<ResponseBody> getDeviceMediaUrl(@Body RequestBody info);

    @POST("/open/u/cloud/getCloudFolderList")
    Call<ResponseBody> getCloudFolderList(@Body RequestBody info);

    @POST("/open/u/cloud/getCloudFileList")
    Call<ResponseBody> getCloudFileList(@Body RequestBody info);

    @POST("/open/u/cloud/getFileUrlById")
    Call<ResponseBody> getFileUrlById(@Body RequestBody info);

    @POST("/api/deviceMsgSubOrUnSub")
    Call<ResponseBody> deviceMsgSubOrUnSub(@Body RequestBody info);


    @POST("/open/oauth/getAccessToken")
    Call<ResponseBody> getAccessToken(@Body RequestBody info);
    /**
     * 云台设备控制
     * @param info
     * @return
     */
    @POST("/open/token/vpaas/device/deviceControl")
    Call<ResponseBody> deviceControl(@Body RequestBody info);


}
