package com.github.tysl_assistant.io;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ZLMediaServer {

    /**
     * 添加rtsp/rtmp/hls拉流代理(addStreamProxy)addStreamProxyCustomWithMp4
     * @param secret
     * @param app
     * @param stream
     * @return
     */
    @GET("/index/api/addStreamProxyCustomWithMp4")
    Call<ResponseBody> addStreamProxyCustomWithMp4(@Query("secret") String secret,@Query("vhost") String vhost,
                                           @Query("app") String app, @Query("stream") String stream, @Query("url") String url);

    /**
     * 修改mp4转流的跳转
     * @param secret
     * @param app
     * @param stream
     * @return
     */
    @GET("/index/api/changeMp4Seek")
    Call<ResponseBody> changeMp4Seek(@Query("secret") String secret,@Query("vhost") String vhost,
                                      @Query("app") String app, @Query("stream") String stream, @Query("seek") Long seek);

    /**
     * 修改mp4转流的跳转
     * @param secret
     * @param app
     * @param stream
     * @return
     */
    @GET("/index/api/changeMp4Speed")
    Call<ResponseBody> changeMp4Speed(@Query("secret") String secret,@Query("vhost") String vhost,
                                     @Query("app") String app, @Query("stream") String stream, @Query("speed") Double speed);

}
