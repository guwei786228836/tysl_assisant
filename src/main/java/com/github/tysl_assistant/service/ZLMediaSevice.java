package com.github.tysl_assistant.service;

import cn.hutool.core.util.StrUtil;
import com.github.tysl_assistant.dto.zlmedia.request.AddStreamProxyZLRequestDTO;
import com.github.tysl_assistant.dto.zlmedia.request.ChangeMp4SeekZLRequestDTO;
import com.github.tysl_assistant.dto.zlmedia.request.ChangeMp4SpeedRequestDTO;
import com.github.tysl_assistant.dto.zlmedia.response.AddStreamProxyZLResponseDTO;
import com.github.tysl_assistant.dto.zlmedia.response.ChangeMp4SeekZLResponseDTO;
import com.github.tysl_assistant.io.ZLMediaServer;
import com.github.tysl_assistant.util.CommonConstants;
import com.github.tysl_assistant.util.json.jackson.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.ResponseBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

import java.io.IOException;

@Service
@Slf4j
public class ZLMediaSevice {

    @Value("${zlMedia.ip}")
    private String ZLUrl;

    @Value("${zlMedia.port}")
    private String ZLPort;

    @Value("${zlMedia.secret}")
    private String ZLSecret;

    @Value("${zlMedia.vhost}")
    private String ZLVhost;


    public ZLMediaServer initZLMediaServer() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl("http://"+ZLUrl + CommonConstants.Common.COLON + ZLPort + CommonConstants.Common.SLASH).build();
        return retrofit.create(ZLMediaServer.class);
    }


    /**
     *添加rtsp/rtmp/hls拉流代理(addStreamProxy)
     * @param requestDTO
     * @return
     */
    public boolean addStreamProxyCustomWithMp4(AddStreamProxyZLRequestDTO requestDTO) {

        if(StrUtil.isEmpty(requestDTO.getApp())){
            return false;
        }

        if(StrUtil.isEmpty(requestDTO.getStream())){
            return false;
        }

        if(StrUtil.isEmpty(requestDTO.getUrl())){
            return false;
        }

        ZLMediaServer zlMediaServer = this.initZLMediaServer();

        Call<ResponseBody> responseBodyCall = zlMediaServer.addStreamProxyCustomWithMp4(ZLSecret, ZLVhost, requestDTO.getApp(),
                requestDTO.getStream(), requestDTO.getUrl());
        try {
            Response<ResponseBody> callResponse = responseBodyCall.execute();
            if (callResponse.isSuccessful()) {
                String temp = callResponse.body().string();
                String content =temp.replaceAll("\n", "").replaceAll("\t", "");
                AddStreamProxyZLResponseDTO dto = JacksonUtil.string2Bean(content, AddStreamProxyZLResponseDTO.class);
                log.info("请求：" +JacksonUtil.objectToString(requestDTO)+"####结果："+JacksonUtil.objectToString(dto));
                if (dto.getCode() == 0) {
                    return true;
                }
            }
        } catch (IOException e) {
            log.info("addStreamProxy出错：", e);
        }
        return false;
    }


    /**
     *修改mp4转流的跳转
     * @param requestDTO
     * @return
     */
    public boolean changeMp4Seek(ChangeMp4SeekZLRequestDTO requestDTO) {

        if(StrUtil.isEmpty(requestDTO.getApp())){
            return false;
        }

        if(StrUtil.isEmpty(requestDTO.getStream())){
            return false;
        }

        if(null == requestDTO.getSeek()){
            return false;
        }

        ZLMediaServer zlMediaServer = this.initZLMediaServer();

        Call<ResponseBody> responseBodyCall = zlMediaServer.changeMp4Seek(ZLSecret, ZLVhost, requestDTO.getApp(),
                requestDTO.getStream(), requestDTO.getSeek());
        try {
            Response<ResponseBody> callResponse = responseBodyCall.execute();
            if (callResponse.isSuccessful()) {
                String content = callResponse.body().string();
                ChangeMp4SeekZLResponseDTO dto = JacksonUtil.string2Bean(content, ChangeMp4SeekZLResponseDTO.class);
                log.info("请求：" +JacksonUtil.objectToString(requestDTO)+"####结果："+JacksonUtil.objectToString(dto));
                if (dto.getCode() == 0) {
                    return true;
                }
            }
        } catch (IOException e) {
            log.info("changeMp4Seek出错：", e);
        }
        return false;
    }


    /**
     *修改mp4转流的速度
     * @param requestDTO
     * @return
     */
    public boolean changeMp4Speed(ChangeMp4SpeedRequestDTO requestDTO) {

        if(StrUtil.isEmpty(requestDTO.getApp())){
            return false;
        }

        if(StrUtil.isEmpty(requestDTO.getStream())){
            return false;
        }

        if(null == requestDTO.getSpeed()){
            return false;
        }

        ZLMediaServer zlMediaServer = this.initZLMediaServer();

        Call<ResponseBody> responseBodyCall = zlMediaServer.changeMp4Speed(ZLSecret, ZLVhost, requestDTO.getApp(),
                requestDTO.getStream(), requestDTO.getSpeed());
        try {
            Response<ResponseBody> callResponse = responseBodyCall.execute();
            if (callResponse.isSuccessful()) {
                String content = callResponse.body().string();
                ChangeMp4SeekZLResponseDTO dto = JacksonUtil.string2Bean(content, ChangeMp4SeekZLResponseDTO.class);
                log.info("请求：" +JacksonUtil.objectToString(requestDTO)+"####结果："+JacksonUtil.objectToString(dto));
                if (dto.getCode() == 0) {
                    return true;
                }
            }
        } catch (IOException e) {
            log.info("changeMp4Speed出错：", e);
        }
        return false;
    }


}
