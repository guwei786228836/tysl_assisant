package com.github.tysl_assistant.service;

import cn.hutool.crypto.SecureUtil;
import cn.hutool.crypto.digest.HMac;
import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import com.github.tysl_assistant.io.TyslServer;
import com.github.tysl_assistant.util.CommonConstants;
import com.github.tysl_assistant.util.secure.XXTeaUtil;
import okhttp3.FormBody;
import okhttp3.RequestBody;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import retrofit2.Retrofit;

@Service
public abstract class AbstractTyslService implements ITyslService{

    @Value("${tysl.url-domain-media}")
    protected String tyslUrl;

    @Value("${tysl.third-app-domain}")
    protected String thirdAppUrl;

    @Value("${tysl.appid}")
    protected String tyslAppId;

    @Value("${tysl.appsecret}")
    protected String appSecret;

    @Value("${tysl.version}")
    protected String appVersion;

    @Value("${tysl.clientType}")
    protected String appClientType;


    @Value("${tysl.RSA-prikey}")
    protected String RSAPrikey;

    @Value("${tysl.phone}")
    protected String tyslPhone;

    public TyslServer getTyslServer() {
        Retrofit retrofit = new Retrofit.Builder().baseUrl(tyslUrl).build();
        return retrofit.create(TyslServer.class);
    }


    public TyslRequestDTO initTyslRequestDTO(long timestamp,TyslRequestDTO tyslRequestDTO) {
        tyslRequestDTO.setAppId(tyslAppId);
        tyslRequestDTO.setClientType(appClientType);
        tyslRequestDTO.setVersion(appVersion);
        tyslRequestDTO.setTimestamp(timestamp + "");
        tyslRequestDTO.setPhone(tyslPhone);
        return tyslRequestDTO;
    }

    public StringBuilder handleFinal(StringBuilder sb, long timestamp) {
        sb.append(TyslRequestDTO.Fields.appId + CommonConstants.Common.EQUAL).append(tyslAppId).append(CommonConstants.Common.AND);
        sb.append(TyslRequestDTO.Fields.version + CommonConstants.Common.EQUAL).append(appVersion).append(CommonConstants.Common.AND);
        sb.append(TyslRequestDTO.Fields.clientType + CommonConstants.Common.EQUAL).append(appClientType).append(CommonConstants.Common.AND);
        sb.append(TyslRequestDTO.Fields.timestamp + CommonConstants.Common.EQUAL).append(timestamp);
        return sb;
    }

    public RequestBody handleRequestBody(TyslRequestDTO tyslRequestDTO, StringBuilder sb, long timestamp){
        String params = XXTeaUtil.encrypt(appSecret,sb.toString());
        HMac hMac = SecureUtil.hmacSha256(appSecret);
        String signature = hMac.digestHex(tyslAppId + appClientType + params + timestamp + appVersion);

        tyslRequestDTO.setParams(params);
        tyslRequestDTO.setSignature(signature);

        FormBody.Builder builder = new FormBody.Builder();
        builder.add(TyslRequestDTO.Fields.params, tyslRequestDTO.getParams());
        builder.add(TyslRequestDTO.Fields.signature, tyslRequestDTO.getSignature());
        builder.add(TyslRequestDTO.Fields.appId, tyslRequestDTO.getAppId());
        builder.add(TyslRequestDTO.Fields.version, tyslRequestDTO.getVersion());
        builder.add(TyslRequestDTO.Fields.clientType, tyslRequestDTO.getClientType());
        builder.add(TyslRequestDTO.Fields.timestamp, tyslRequestDTO.getTimestamp());

        return builder.build();
    }

}
