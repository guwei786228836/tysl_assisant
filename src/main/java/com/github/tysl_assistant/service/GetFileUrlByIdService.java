package com.github.tysl_assistant.service;

import cn.hutool.core.util.StrUtil;
import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import com.github.tysl_assistant.dto.tysl.request.tysl.GetFileUrlByIdDTO;
import com.github.tysl_assistant.dto.tysl.response.GetFileUrlByIdReponseDTO;
import com.github.tysl_assistant.dto.tysl.response.TyslResultDTO;
import com.github.tysl_assistant.io.TyslServer;
import com.github.tysl_assistant.util.CommonConstants;
import com.github.tysl_assistant.util.json.jackson.JacksonUtil;
import com.github.tysl_assistant.util.secure.RSAUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;

@Service
@Slf4j
public class GetFileUrlByIdService extends AbstractTyslService {

    @Override
    public String getResponse(TyslRequestDTO tyslRequestDTO) {
        TyslServer tyslServer = this.getTyslServer();

        long timestamp = System.currentTimeMillis();
        tyslRequestDTO = this.initTyslRequestDTO(timestamp, tyslRequestDTO);


        GetFileUrlByIdDTO getFileUrlByIdDTO = (GetFileUrlByIdDTO) tyslRequestDTO;

        StringBuilder sb = new StringBuilder();
        sb.append(TyslRequestDTO.Fields.phone + CommonConstants.Common.EQUAL).append(getFileUrlByIdDTO.getPhone()).append(CommonConstants.Common.AND);
        sb.append(GetFileUrlByIdDTO.Fields.deviceCode + CommonConstants.Common.EQUAL).append(getFileUrlByIdDTO.getDeviceCode()).append(CommonConstants.Common.AND);
        sb.append(GetFileUrlByIdDTO.Fields.id + CommonConstants.Common.EQUAL).append(getFileUrlByIdDTO.getId()).append(CommonConstants.Common.AND);
        sb = this.handleFinal(sb, timestamp);

        RequestBody body = this.handleRequestBody(tyslRequestDTO, sb, timestamp);

        Call<ResponseBody> pisPicCall = tyslServer.getFileUrlById(body);
        Response<ResponseBody> pisPicCallResponse;
        try {
            pisPicCallResponse = pisPicCall.execute();
            if (pisPicCallResponse.isSuccessful()) {
                String content = pisPicCallResponse.body().string();
                TyslResultDTO tyslResultDTO = JacksonUtil.string2Bean(content, TyslResultDTO.class);
                if (0 == tyslResultDTO.getCode() && StrUtil.isNotEmpty(tyslResultDTO.getData())) {
                    content = RSAUtil.decrypt(RSAPrikey, tyslResultDTO.getData());
                    GetFileUrlByIdReponseDTO getFileUrlByIdReponseDTO = JacksonUtil.string2Bean(content, GetFileUrlByIdReponseDTO.class);
                    log.info("GetFileUrlByIdService:"+JacksonUtil.objectToString(getFileUrlByIdReponseDTO));
                    return getFileUrlByIdReponseDTO.getUrl();
                } else {
                    log.info("GetCloudFileListService 返回错误:" + content);
                    return null;
                }
            }
        } catch (IOException e) {
            log.info("GetFileUrlByIdService 报错", e);
        }
        log.info("GetFileUrlByIdService 错误结束");
        return null;
    }

}
