package com.github.tysl_assistant.service;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.date.DateUnit;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.github.tysl_assistant.dto.tysl.AddStreamProxyRequestDTO;
import com.github.tysl_assistant.dto.tysl.GetNextFileRequsetDTO;
import com.github.tysl_assistant.dto.tysl.request.tysl.GetCloudFileListRequestDTO;
import com.github.tysl_assistant.dto.tysl.request.tysl.GetFileUrlByIdDTO;
import com.github.tysl_assistant.dto.tysl.response.GetCloudFileListReponseDTO;
import com.github.tysl_assistant.dto.tysl.response.GetFileUrlByIdReponseDTO;
import com.github.tysl_assistant.util.MyDateUtil;
import com.github.tysl_assistant.util.json.jackson.JacksonUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class TyslService {

    @Resource
    private GetCloudFileListService getCloudFileListService;

    @Resource
    private ZLMediaSevice zlMediaSevice;

    @Resource
    private GetFileUrlByIdService getFileUrlByIdService;

    @Value("${savePath}")
    private String savePath;

    @Value("${zlMedia.rstp}")
    private  String rtspPort;


    public GetFileUrlByIdReponseDTO addStreamProxy(AddStreamProxyRequestDTO addStreamProxyRequestDTO) throws IOException {
        if (StrUtil.isEmpty(addStreamProxyRequestDTO.getId())) {
            return null;
        }
        if (StrUtil.isEmpty(addStreamProxyRequestDTO.getCode())) {
            return null;
        }
        if (StrUtil.isEmpty(addStreamProxyRequestDTO.getRequestTime())) {
            return null;
        }
        Date requestTimeDate = DateUtil.parse(addStreamProxyRequestDTO.getRequestTime());
        if (null == requestTimeDate) {
            return null;
        }

        GetNextFileRequsetDTO getNextFileRequsetDTO = new GetNextFileRequsetDTO();
        BeanUtil.copyProperties(addStreamProxyRequestDTO, getNextFileRequsetDTO);
        return  this.getNextFile(getNextFileRequsetDTO);
    }


    public GetFileUrlByIdReponseDTO getNextFile(GetNextFileRequsetDTO getNextFileRequsetDTO) {

        if (StrUtil.isEmpty(getNextFileRequsetDTO.getId())) {
            log.info("id为空：" + JacksonUtil.objectToString(getNextFileRequsetDTO));
            return null;
        }
        GetCloudFileListRequestDTO getCloudFileListRequestDTO = new GetCloudFileListRequestDTO();
        getCloudFileListRequestDTO.setDeviceCode(getNextFileRequsetDTO.getCode());
        Date   requestTimeDate = DateUtil.parse( getNextFileRequsetDTO.getRequestTime(),MyDateUtil.DATE_TIME_FORMAT_STANDARD_OTHER);

        getCloudFileListRequestDTO.setPath(DateUtil.format(requestTimeDate, MyDateUtil.DATE_FORMAT_WITHOUT_HORIZONTAL_BAR));
        getCloudFileListRequestDTO.setStartDate(DateUtil.format(requestTimeDate, MyDateUtil.DATE_TIME_FORMAT_STANDARD_WITHOUT_SECOND_OTHER));
        getCloudFileListRequestDTO.setEndDate(DateUtil.format(requestTimeDate, MyDateUtil.DATE_TIME_FORMAT_STANDARD_WITHOUT_SECOND_OTHER));
        getCloudFileListRequestDTO.setType("2");

        List<GetCloudFileListReponseDTO> list = getCloudFileListService.getResponse(getCloudFileListRequestDTO);
        GetCloudFileListReponseDTO getCloudFileListReponseDTO = list.get(0);
        log.info("tysl返回文件列表：" + JacksonUtil.objectToString(list));

        GetFileUrlByIdDTO getFileUrlByIdDTO = new GetFileUrlByIdDTO();
        getFileUrlByIdDTO.setDeviceCode(getCloudFileListRequestDTO.getDeviceCode());
        getFileUrlByIdDTO.setId(getCloudFileListReponseDTO.getId());

        String url = getFileUrlByIdService.getResponse(getFileUrlByIdDTO);
        if(StrUtil.isEmpty(url)){
            return  null;
        }

        Date lastOpTime  = getCloudFileListReponseDTO.getLastOpTime();

        Long seconds = DateUtil.between(requestTimeDate,lastOpTime, DateUnit.SECOND);

        if(seconds <0 || seconds > 290){
            seconds = 0L;
        }

        GetFileUrlByIdReponseDTO reponseDTO = new GetFileUrlByIdReponseDTO();
        reponseDTO.setUrl(url);
        reponseDTO.setSeekSeconds(seconds);




        return reponseDTO;
    }




}
