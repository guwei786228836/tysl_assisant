package com.github.tysl_assistant.service;


import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import com.github.tysl_assistant.dto.tysl.response.TyslResultDTO;
import com.github.tysl_assistant.io.TyslServer;
import okhttp3.RequestBody;

import java.util.Map;

public interface ITyslService {

    Object getResponse(TyslRequestDTO tyslRequestDTO);

    TyslServer getTyslServer();

    TyslRequestDTO initTyslRequestDTO(long timestamp,TyslRequestDTO tyslRequestDTO);

     StringBuilder handleFinal(StringBuilder sb, long timestamp);

    RequestBody handleRequestBody(TyslRequestDTO tyslRequestDTO, StringBuilder sb, long timestamp);

}
