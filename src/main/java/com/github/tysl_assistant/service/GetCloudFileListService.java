package com.github.tysl_assistant.service;

import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.tysl_assistant.dto.tysl.TyslRequestDTO;
import com.github.tysl_assistant.dto.tysl.request.tysl.GetCloudFileListRequestDTO;
import com.github.tysl_assistant.dto.tysl.response.GetCloudFileListReponseDTO;
import com.github.tysl_assistant.dto.tysl.response.TyslListResultDTO;
import com.github.tysl_assistant.dto.tysl.response.TyslResultDTO;
import com.github.tysl_assistant.io.TyslServer;
import com.github.tysl_assistant.util.CommonConstants;
import com.github.tysl_assistant.util.json.jackson.JacksonUtil;
import com.github.tysl_assistant.util.secure.RSAUtil;
import lombok.extern.slf4j.Slf4j;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import org.springframework.stereotype.Service;
import retrofit2.Call;
import retrofit2.Response;

import java.io.IOException;
import java.util.List;

@Service
@Slf4j
public class GetCloudFileListService  extends AbstractTyslService{

    public List<GetCloudFileListReponseDTO> getResponse(TyslRequestDTO tyslRequestDTO) {
        TyslServer tyslServer =  this.getTyslServer();

        long timestamp = System.currentTimeMillis();
        tyslRequestDTO = this.initTyslRequestDTO(timestamp,tyslRequestDTO);

        GetCloudFileListRequestDTO getCloudFileListRequestDTO = (GetCloudFileListRequestDTO)tyslRequestDTO;
        StringBuilder sb = new StringBuilder();
        sb.append(TyslRequestDTO.Fields.phone + CommonConstants.Common.EQUAL).append(getCloudFileListRequestDTO.getPhone()).append(CommonConstants.Common.AND);
        sb.append(GetCloudFileListRequestDTO.Fields.deviceCode + CommonConstants.Common.EQUAL).append(getCloudFileListRequestDTO.getDeviceCode()).append(CommonConstants.Common.AND);
        sb.append(GetCloudFileListRequestDTO.Fields.path + CommonConstants.Common.EQUAL).append(getCloudFileListRequestDTO.getPath()).append(CommonConstants.Common.AND);
        sb.append(GetCloudFileListRequestDTO.Fields.type + CommonConstants.Common.EQUAL).append(getCloudFileListRequestDTO.getType()).append(CommonConstants.Common.AND);
        sb.append(GetCloudFileListRequestDTO.Fields.startDate + CommonConstants.Common.EQUAL).append(getCloudFileListRequestDTO.getStartDate()).append(CommonConstants.Common.AND);
        sb.append(GetCloudFileListRequestDTO.Fields.endDate + CommonConstants.Common.EQUAL).append(getCloudFileListRequestDTO.getEndDate()).append(CommonConstants.Common.AND);
        sb = this.handleFinal(sb, timestamp);

        RequestBody body = this.handleRequestBody(tyslRequestDTO,sb,timestamp);

        Call<ResponseBody> pisPicCall = tyslServer.getCloudFileList(body);
        Response<ResponseBody> pisPicCallResponse;
        try {
            pisPicCallResponse = pisPicCall.execute();
            if (pisPicCallResponse.isSuccessful()) {
                String content = pisPicCallResponse.body().string();
                TyslResultDTO tyslResultDTO = JacksonUtil.string2Bean(content, TyslResultDTO.class);
                if(0 == tyslResultDTO.getCode()){
                    content = RSAUtil.decrypt(RSAPrikey, tyslResultDTO.getData());
                    ObjectMapper mapper = new ObjectMapper();
                    mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                    TyslListResultDTO<GetCloudFileListReponseDTO> resultDTO = mapper.readValue(content, new TypeReference<TyslListResultDTO<GetCloudFileListReponseDTO>>() {});
                    if(CollectionUtil.isEmpty(resultDTO.getList())){
                        log.info("GetCloudFileListService 返回为空:"+content);
                        return null;
                    }
                    return resultDTO.getList();
                }else{
                    log.info("GetCloudFileListService 返回错误:"+content);
                    return null;
                }
            }
        } catch (IOException e) {
            log.info("GetCloudFileListService 报错",e);
        }
        log.info("GetFileUrlByIdService 错误结束");
        return null;
    }

}
